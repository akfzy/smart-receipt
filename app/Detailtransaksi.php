<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detailtransaksi extends Model
{
  protected $table = 'transaction_details';

  public $timestamps = false;
}
