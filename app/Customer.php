<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'users';

    public $timestamps = false;

    protected $hidden = [
        'password', 'id_telegram'
    ];
}
