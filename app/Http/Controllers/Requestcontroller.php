<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Customer;
use App\Store;
use App\Transaksi;
use App\Detailtransaksi;

class Requestcontroller extends Controller
{
    public function index(Request $req, $username){
        $cust = Customer::where('username', $username)->first();
        if($cust){
            return $cust;
        }else{
            return response('User Not Found', 404);
        }
    }

    public function dataResi(Request $req){
        $customer = Customer::where('username', $req->input('customer'))->first();
        $store = Store::where('username', $req->input('store'))->first();

        $transaksi = new Transaksi;
        $transaksi->user = $customer->id;
        $transaksi->store = $store->id;
        $transaksi->bayar = $req->input('bayar');
        $transaksi->total = $req->input('total');
        $transaksi->kembalian = $req->input('kembalian');
        $transaksi->save();

        foreach ($req->input('DataResi') as $key => $value) {
          $detail = new Detailtransaksi;
          $detail->transaction = $transaksi->id;
          $detail->item = $value['nama'];
          $detail->harga = $value['harga'];
          $detail->jumlah = $value['jumlah'];
          $detail->barcode = $value['barcode'];
          $detail->subtotal = $value['subtotal'];
          $detail->save();
        }

        // TODO: Add action setelah transaksi berhasil

    }

    public function Notifikasi(Request $req){

    }
}
