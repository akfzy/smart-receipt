<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Store;

class Apiauth extends Controller
{
    public function index(Request $req){
        $store = Store::where('username', $req->username)->first();

        if(!$store || !Hash::check($req->password, $store->password)){
            return response(['message' => ['Invalid Login']], 400);
        }

        $token = $store->createToken('NodeMCUToken');

        return response($token->plainTextToken, 200);
    }
}
