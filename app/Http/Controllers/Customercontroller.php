<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Customer;

class Customercontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userdata = Customer::all();
        return view('client')->with('data', $userdata);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'username' => 'required|unique:App\Customer,username|max:30',
            'password' => 'required|min:6|max:32|alpha_num',
            'email' => 'required|email|unique:App\Customer,email|max:255',
            'nama_lengkap' => 'required|max:255',
            'alamat' => 'required',
            'kota' => 'required|max:100',
            'kodepos' => 'required|max:10',
            'id_telegram' => 'required|max:20'
        ]);

        $customer = new Customer;
        $customer->username = $request->input('username');
        $customer->password = Hash::make($request->input('password'));
        $customer->email = $request->input('email');
        $customer->nama_lengkap = $request->input('nama_lengkap');
        $customer->alamat = $request->input('alamat');
        $customer->kota = $request->input('kota');
        $customer->kodepos = $request->input('kodepos');
        $customer->id_telegram = $request->input('id_telegram');

        $customer->save();
        
        return redirect('/customer');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $customerData = Customer::find($id);
        return view('individual')->with('data', $customerData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $customerData = Customer::find($id);
        return view('edit')->with('data', $customerData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validatedData = $request->validate([
            'username' => 'required|max:30|unique:App\Customer,username,'.$id,
            'email' => 'required|email|max:255|unique:App\Customer,email,'.$id,
            'nama_lengkap' => 'required|max:255',
            'alamat' => 'required',
            'kota' => 'required|max:100',
            'kodepos' => 'required|max:10',
            'id_telegram' => 'required|max:20',
        ]);

        $customer = Customer::find($id);
        $customer->username = $request->input('username');
        $customer->email = $request->input('email');
        $customer->nama_lengkap = $request->input('nama_lengkap');
        $customer->alamat = $request->input('alamat');
        $customer->kota = $request->input('kota');
        $customer->kodepos = $request->input('kodepos');
        $customer->id_telegram = $request->input('id_telegram');

        $customer->save();
        
        return redirect('/customer')->with('success', 'Data berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
