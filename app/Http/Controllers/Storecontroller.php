<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Store;

class Storecontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $stores = Store::all();
        return view('store.stores')->with('data', $stores);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('store.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $request->validate([
            'username' => 'required|unique:App\Store,username|max:30',
            'password' => 'required|min:6|max:32|alpha_num',
            'email' => 'required|email|unique:App\Store,email|max:255',
            'nama_toko' => 'required|max:255',
            'alamat' => 'required',
            'kota' => 'required|max:100',
            'kodepos' => 'required|max:10'
        ]);

        $store = new Store;
        $store->username = $request->input('username');
        $store->password = Hash::make($request->input('password'));
        $store->email = $request->input('email');
        $store->nama_toko = $request->input('nama_toko');
        $store->alamat = $request->input('alamat');
        $store->kota = $request->input('kota');
        $store->kodepos = $request->input('kodepos');

        $store->save();

        return redirect('/store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $stores = Store::find($id);
        return view('store.individual')->with('data', $stores);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $stores = Store::find($id);
        return view('store.edit')->with('data', $stores);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validatedData = $request->validate([
            'username' => 'required|max:30|unique:App\Store,username,'.$id,
            'email' => 'required|email|max:255|unique:App\Store,email,'.$id,
            'nama_toko' => 'required|max:255',
            'alamat' => 'required',
            'kota' => 'required|max:100',
            'kodepos' => 'required|max:10'
        ]);

        $store = Store::find($id);
        $store->username = $request->input('username');
        $store->email = $request->input('email');
        $store->nama_toko = $request->input('nama_toko');
        $store->alamat = $request->input('alamat');
        $store->kota = $request->input('kota');
        $store->kodepos = $request->input('kodepos');

        $store->save();
        
        return redirect('/store')->with('success', 'Data berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
