<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Transaksi;
use App\Detailtransaksi;
Use App\User;

class Transaksicontroller extends Controller
{
    public function index(){
      $data['trans'] = Transaksi::all();
      foreach ($data['trans'] as $key => $value) {
        $data['customer'] = $value->customer;
        $data['stores'] = $value->stores;
      }

      return view('transaksi.transaksi')->with('data', $data);
    }

    public function details($id){
      $data['trans'] = Transaksi::find($id);
      $data['transaksi'] = Detailtransaksi::where('transaction', $id)->get();
      $data['customer'] = $data['trans']->customer;
      $data['stores'] = $data['trans']->stores;

      return view('transaksi.details')->with('data', $data);
    }

}
