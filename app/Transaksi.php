<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = 'transactions';

    public $timestamps = false;

    public function customer(){
      return $this->hasMany('App\User', 'id', 'user');
    }

    public function stores(){
      return $this->hasMany('App\Store', 'id', 'store');
    }
}
