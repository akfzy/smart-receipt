<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;

class Store extends Model
{
    use HasApiTokens;

    protected $table = 'stores';

    public $timestamps = false;

    protected $hidden = [
        'password'
    ];
}
