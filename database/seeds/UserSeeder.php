<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
            'nama_lengkap' => Str::random(10),
            'alamat' => Str::random(10),
            'kodepos' => Str::random(8),
            'kota' => Str::random(10),
            'avatar' => Str::random(10),
            'id_telegram' => Str::random(10)
        ]);
    }
}
