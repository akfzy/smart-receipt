<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('customer', 'Customercontroller');
Route::resource('store', 'Storecontroller');

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/auth/local', 'Apiauth@index');

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/request/{username}', 'Requestcontroller@index');
    Route::post('/request/resi', 'Requestcontroller@dataResi');
    Route::post('/request/notifikasi', 'Requestcontroller@Notifikasi');
});

Route::get('/transaksi', 'Transaksicontroller@index');
Route::get('/transaksi/{id}', 'Transaksicontroller@details');
