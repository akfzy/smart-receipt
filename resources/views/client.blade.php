@extends('layout.main')

@section('custom-css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
@endsection

@section('title', 'Client Area')

@section('page-heading')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Client Area</h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <table id="table_id" class="display">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Nama Lengkap</th>
                        <th>Alamat</th>
                        <th>Kota</th>
                        <th>Kode Pos</th>
                        <th>Avatar</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $item)
                        <tr>
                            <td>
                                {{$item->id}}
                            </td>
                            <td>
                                {{$item->username}}
                            </td>
                            <td>
                                {{$item->email}}
                            </td>
                            <td>
                                {{$item->nama_lengkap}}
                            </td>
                            <td>
                                {{$item->alamat}}
                            </td>
                            <td>
                                {{$item->kota}}
                            </td>
                            <td>
                                {{$item->kodepos}}
                            </td>
                            <td>
                                {{$item->avatar}}
                            </td>
                            <td>
                                <a class="btn btn-info" href="/customer/{{$item->id}}"> Detail </a>
                                <a class="btn btn-warning" href="/customer/{{$item->id}}/edit"> Edit </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>    
    </div>
@endsection

@section('custom-js')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready( function () {
            $('#table_id').DataTable({
                "columnDefs": [
                    { "orderable": false, "targets": -1 }
                ]
            });
        });
    </script>
@endsection