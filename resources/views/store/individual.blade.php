@extends('layout.main')

@section('title', 'Store Area')

@section('page-heading')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Store Data</h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(session('success'))
                <div class="alert alert-sucess mb-2" role="alert">
                    {{session('success')}}
                </div>
            @endif
            <div class="card mb-4">
                <div class="card-header">
                    {{$data->nama_toko}} Details
                </div>
                <div class="card-body">
                  <table>
                      <tr>
                          <td>Username</td>
                          <td> : {{$data->username}}</td>
                      </tr>
                      <tr>
                          <td>Nama Toko</td>
                          <td> : {{$data->nama_toko}}</td>
                      </tr>
                      <tr>
                          <td>Email</td>
                          <td> : {{$data->email}}</td>
                      </tr>
                      <tr>
                          <td>Alamat</td>
                          <td> : {{$data->alamat}}</td>
                      </tr>
                      <tr>
                          <td>kota</td>
                          <td> : {{$data->kota}}</td>
                      </tr>
                      <tr>
                          <td>Kode Pos</td>
                          <td> : {{$data->kodepos}}</td>
                      </tr>
                  </table>
                </div>
            </div>
        </div>
    </div>
@endsection