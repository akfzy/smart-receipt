@extends('layout.main')

@section('title', 'Store Area')

@section('page-heading')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Register Store / Market</h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(count($errors) > 0)
               @foreach ($errors->all() as $error)
                <div class="alert alert-danger mb-2" role="alert">
                    {{$error}}
                </div>
               @endforeach 
            @endif
            <div class="card mb-4">
                <div class="card-header">
                  Data Customer
                </div>
                <div class="card-body">
                    {!! Form::open(['action' => 'Storecontroller@store', 'method' => 'post']) !!}
                        {{Form::text('username', '',['class' => 'form-control mb-4', 'placeholder' => 'Username'])}}
                        {{Form::password('password', ['class' => 'form-control mb-4', 'placeholder' => 'Password'])}}
                        {{Form::text('email', '',['class' => 'form-control mb-4', 'placeholder' => 'E-mail'])}}
                        {{Form::text('nama_toko', '',['class' => 'form-control mb-4', 'placeholder' => 'Nama Toko'])}}
                        {{Form::textarea('alamat', '',['class' => 'form-control mb-4', 'placeholder' => 'Alamat'])}}
                        {{Form::text('kota', '',['class' => 'form-control mb-4', 'placeholder' => 'Kota'])}}
                        {{Form::text('kodepos', '',['class' => 'form-control mb-4', 'placeholder' => 'Kode Pos'])}}
                        {{Form::submit('SUBMIT', ['class' => 'btn btn-block btn-primary'])}}
                    {!! Form::close() !!}
                </div>
            </div>            
        </div>
    </div>
@endsection