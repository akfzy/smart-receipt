@extends('layout.main')

@section('title', 'Client Area')

@section('page-heading')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Edit Client / Customer Data</h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(count($errors) > 0)
               @foreach ($errors->all() as $error)
                <div class="alert alert-danger mb-2" role="alert">
                    {{$error}}
                </div>
               @endforeach 
            @endif
            <div class="card mb-4">
                <div class="card-header">
                  Edit Data Customer
                </div>
                <div class="card-body">
                    {!! Form::open(['action' => ['Customercontroller@update', $data->id], 'method' => 'post']) !!}
                        {{Form::text('username', $data->username,['class' => 'form-control mb-4', 'placeholder' => 'Username'])}}
                        {{Form::text('email', $data->email,['class' => 'form-control mb-4', 'placeholder' => 'E-mail'])}}
                        {{Form::text('nama_lengkap', $data->nama_lengkap,['class' => 'form-control mb-4', 'placeholder' => 'Nama Lengkap'])}}
                        {{Form::textarea('alamat', $data->alamat,['class' => 'form-control mb-4', 'placeholder' => 'Alamat'])}}
                        {{Form::text('kota', $data->kota,['class' => 'form-control mb-4', 'placeholder' => 'Kota'])}}
                        {{Form::text('kodepos', $data->kodepos,['class' => 'form-control mb-4', 'placeholder' => 'Kode Pos'])}}
                        {{Form::text('id_telegram', $data->id_telegram,['class' => 'form-control mb-4', 'placeholder' => 'ID Telegram'])}}
                        {!! Form::hidden('_method', 'PUT') !!}
                        {{Form::submit('SUBMIT', ['class' => 'btn btn-block btn-primary'])}}
                    {!! Form::close() !!}
                </div>
            </div>            
        </div>
    </div>
@endsection