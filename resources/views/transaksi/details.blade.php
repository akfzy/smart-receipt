@extends('layout.main')

@section('title', 'Client Area')

@section('custom-css')
  <link rel="stylesheet" href="{{ URL::to('/') }}/css/details.css">
@endsection

@section('page-heading')
  <!-- Page Heading -->
  <h1 class="h3 mb-4 text-gray-800">Transaction Data</h1>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        @if(session('success'))
          <div class="alert alert-sucess mb-2" role="alert">
              {{session('success')}}
          </div>
        @endif
        <div class="card mb-4">
            <div class="card-header">
                #000{{$data['trans']->id}} Details
            </div>
            <div class="card-body">
                <table>
                    <thead>
                        <tr>
                            <th colspan="4">Invoice #000{{$data['trans']->id}}</th>
                            <th>{{date('d-m-Y', strtotime($data['trans']->waktu))}}</th>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <strong>Pay to:</strong>
                                @foreach($data['stores'] as $stores)
                                  {{$stores->nama_toko}}<br>
                                  {{$stores->alamat}}<br>
                                  {{$stores->kota}}, {{$stores->kodepos}}<br>
                                  {{$stores->email}}<br>
                                @endforeach
                            </td>
                            <td colspan="2">
                                <strong>Customer:</strong><br>
                                @foreach($data['customer'] as $customer)
                                  {{$customer->nama_lengkap}}<br>
                                  {{$customer->alamat}}<br>
                                  {{$customer->kota}}, {{$customer->kodepos}}<br>
                                  {{$customer->email}}<br>
                                @endforeach
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>Qty.</th>
                            <th>Nama</th>
                            <th>Barcode</th>
                            <th>Harga Satuan</th>
                            <th>Subtotal</th>
                        </tr>
                        @foreach($data['transaksi'] as $transaksi)
                          <tr>
                              <td>{{$transaksi->jumlah}}</td>
                              <td>{{$transaksi->item}}</td>
                              <td>{{$transaksi->barcode}}</td>
                              <td>{{$transaksi->harga}}</td>
                              <td>{{$transaksi->subtotal}}</td>
                          </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="4">Total</th>
                            <td>Rp.
                              {{$data['trans']->total}}
                            </td>
                        </tr>
                        <tr>
                            <th colspan="4">Bayar</th>
                            <td>Rp.
                              {{$data['trans']->bayar}}
                            </td>
                        </tr>
                        <tr>
                            <th colspan="4">Kembalian</th>
                            <td>Rp.
                              {{$data['trans']->kembalian}}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
