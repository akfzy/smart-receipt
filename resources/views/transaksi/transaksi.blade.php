@extends('layout.main')

@section('custom-css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
@endsection

@section('title', 'Transactions Area')

@section('page-heading')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Transactions Area</h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <table id="table_id" class="display">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Customer</th>
                        <th>Store</th>
                        <th>Pembayaran</th>
                        <th>Total</th>
                        <th>Kembalian</th>
                        <th>Waktu</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data['trans'] as $item)
                        <tr>
                            <td>
                                {{$item->id}}
                            </td>
                            <td>
                                @foreach($item->customer as $customer)
                                  @if($customer->id == $item->user)
                                    {{$customer->nama_lengkap}}
                                  @endif
                                @endforeach
                            </td>
                            <td>
                              @foreach($item->stores as $store)
                                @if($store->id == $item->store)
                                  {{$store->nama_toko}}
                                @endif
                              @endforeach
                            </td>
                            <td>
                                {{$item->bayar}}
                            </td>
                            <td>
                                {{$item->total}}
                            </td>
                            <td>
                                {{$item->kembalian}}
                            </td>
                            <td>
                                {{date('d-m-Y', strtotime($item->waktu))}}
                            </td>
                            <td>
                                <a class="btn btn-info" href="/transaksi/{{$item->id}}"> Detail </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('custom-js')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready( function () {
            $('#table_id').DataTable({
                "columnDefs": [
                    { "orderable": false, "targets": -1 }
                ]
            });
        });
    </script>
@endsection
